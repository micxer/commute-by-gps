package parser

import (
	"fmt"
	"time"

	"github.com/micxer/commute-by-gps/log"

	"github.com/tkrajina/gpxgo/gpx"
	"gopkg.in/yaml.v2"
)

// GpxParser loads and parses GPX files
type GpxParser struct {
	gpx *gpx.GPX
}

// GpxBounds mimicks the gpx.GpxBounds type
type GpxBounds struct {
	MinLatitude  float64 `yaml:"minlatitude"`
	MaxLatitude  float64 `yaml:"maxlatitude"`
	MinLongitude float64 `yaml:"minlongitude"`
	MaxLongitude float64 `yaml:"maxlongitude"`
}

// Places contains a list of Areas
type Places struct {
	Places []Area `yaml:"places"`
}

// Area is a named place with certain GpxBounds
type Area struct {
	Name   string    `yaml:"name"`
	Bounds GpxBounds `yaml:"bounds"`
}

// Result holds the result data
type Result struct {
	date time.Time
	Name string
}

// EqualNames checks if both Results refere to the same place
func (r *Result) EqualNames(other *Result) bool {
	if other == nil {
		return false
	}
	return r.Name == other.Name
}

// Equals checks if both Results refere to the same place
func (r *Result) Equals(other *Result) bool {
	return r.EqualNames(other) && r.date.Equal(other.date)
}

//String implements Stringer interface
func (r *Result) String() string {
	return fmt.Sprintf("%s, %+v",
		r.date.Format("02.01.2006"),
		r.Name,
	)
}

// LoadPlaces returns a list of areas loaded from a YAML data
func LoadPlaces(yamlData []byte) *Places {
	var places Places

	err := yaml.UnmarshalStrict(yamlData, &places)
	check(err)

	log.Debugf("Loaded places: %v", places)

	return &places
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

// NewParser reads a file and returns a new parser
func NewParser(gpxData []byte) *GpxParser {
	gpx, err := gpx.ParseBytes(gpxData)
	check(err)

	g := GpxParser{gpx}
	return &g
}

//Contains check if the bounds contain a given point
func (b *GpxBounds) Contains(point *gpx.GPXPoint) bool {
	return b.MaxLatitude >= point.Latitude &&
		b.MinLatitude <= point.Latitude &&
		b.MaxLongitude >= point.Longitude &&
		b.MinLongitude <= point.Longitude
}

// IterateTrackPoints returns true if the tracks leads through the given area
func (g *GpxParser) IterateTrackPoints(places *Places) []Result {
	result := []Result{}

	g.gpx.ExecuteOnAllPoints(func(point *gpx.GPXPoint) {
		log.Debugf("Point: %v", point.Point)
		for _, place := range places.Places {
			if place.Bounds.Contains(point) {
				log.Debugf("Found: %s", place.Name)
				result = append(result, Result{point.Timestamp, place.Name})
			}
		}
	})

	return result
}
