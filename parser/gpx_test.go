package parser

import (
	"testing"
)

func TestLoadPlacesYaml(t *testing.T) {
	var yamlData = `
places:
  - name: test
    bounds:
      minlatitude: 1
      maxlatitude: 1
      minlongitude: 1
      maxlongitude: 1
  - name: test2
    bounds:
      minlatitude: 2
      maxlatitude: 2
      minlongitude: 2
      maxlongitude: 2
`
	places := LoadPlaces([]byte(yamlData))

	length := len(places.Places)
	if length != 2 {
		t.Errorf("places len should be 2, got %d", length)
	}
}

func TestLoadPlacesYamlFailsWithMissingData(t *testing.T) {
	t.SkipNow()
	var yamlData = `
places:
  - name: test
  - name: test2
    bounds:
      minlatitude: 2
      maxlatitude: 2
      minlongitude: 2
      maxlongitude: 2
`
	places := LoadPlaces([]byte(yamlData))

	length := len(places.Places)
	t.Logf("%v", places)
	if length != 2 {
		t.Errorf("places len should be 2, got %d", length)
	}
}
