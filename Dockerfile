# Build Stage
FROM lacion/alpine-golang-buildimage:1.13 AS build-stage

LABEL app="build-commute-by-gps"
LABEL REPO="https://github.com/micxer/commute-by-gps"

ENV PROJPATH=/go/src/github.com/micxer/commute-by-gps

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:$GOROOT/bin:$GOPATH/bin

ADD . /go/src/github.com/micxer/commute-by-gps
WORKDIR /go/src/github.com/micxer/commute-by-gps

RUN make build-alpine

# Final Stage
FROM lacion/alpine-base-image:latest

ARG GIT_COMMIT
ARG VERSION
LABEL REPO="https://github.com/micxer/commute-by-gps"
LABEL GIT_COMMIT=$GIT_COMMIT
LABEL VERSION=$VERSION

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:/opt/commute-by-gps/bin

WORKDIR /opt/commute-by-gps/bin

COPY --from=build-stage /go/src/github.com/micxer/commute-by-gps/bin/commute-by-gps /opt/commute-by-gps/bin/
RUN chmod +x /opt/commute-by-gps/bin/commute-by-gps

# Create appuser
RUN adduser -D -g '' commute-by-gps
USER commute-by-gps

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["/opt/commute-by-gps/bin/commute-by-gps"]
