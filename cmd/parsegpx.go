package cmd

import (
	"fmt"
	"io/ioutil"

	"github.com/micxer/commute-by-gps/log"
	"github.com/micxer/commute-by-gps/parser"
	"github.com/spf13/cobra"
)

var parseGpxCmd = &cobra.Command{
	Use:   "parsegpx <track_file> <places_file>",
	Short: "Parses the given file and check for matching areas",
	Long: `Parse a given GPX file and check for waypoints that match a certain area.
These are returned together with date and time.`,
	Args: cobra.ExactValidArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		gpxFile := args[0]
		placesFile := args[1]

		log.Debugf("Loading places from: %s", placesFile)
		yamlData, err := ioutil.ReadFile(placesFile)
		check(err)
		log.Debugf("Loading places from: %s", string(yamlData))
		places := parser.LoadPlaces(yamlData)

		gpxData, err := ioutil.ReadFile(gpxFile)
		check(err)
		parser := parser.NewParser(gpxData)

		results := parser.IterateTrackPoints(places)
		log.Debugf("Results: %v", results)

		printResults(results)
	},
}

func init() {
	rootCmd.AddCommand(parseGpxCmd)
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func printResults(results []parser.Result) {
	fmt.Print("Found the following known places in the track:\n\n")

	var currentPlace *parser.Result
	for _, result := range results {
		if !result.EqualNames(currentPlace) {
			if currentPlace != nil {
				fmt.Printf("%s\n", result.String())
			}
			currentPlace = &result
			fmt.Printf("%s\n", result.String())
		}
	}
	if len(results) > 0 && !currentPlace.Equals(&results[len(results)-1]) {
		fmt.Printf("%s\n", results[len(results)-1].String())
	}
}
