module github.com/micxer/commute-by-gps

go 1.14

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/tkrajina/gpxgo v1.0.1
	gopkg.in/yaml.v2 v2.2.8

)
